from flask import Flask, render_template, Response
import time
import os
import cv2
import numpy as np
from opencv import show

sub = cv2.createBackgroundSubtractorMOG2()


app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(show(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(debug=True)
