import cv2
import time
import os
import numpy as np

path = 'C:\\Users\\sonix\\Downloads\\recognition-master\\recognition-master'

MODEL = cv2.dnn.readNet(
    path + '\\models\\thainum_last.weights',
    path + '\\models\\thainum.cfg'
)

VIDEO = cv2.VideoCapture(0)
_preview = True
_flipH = False
_detect = True
_exposure = VIDEO.get(cv2.CAP_PROP_EXPOSURE)
_contrast = VIDEO.get(cv2.CAP_PROP_CONTRAST)
CLASSES = []
with open(path + "\\models\\tnum.names", "r", encoding='utf8') as f:
    CLASSES = [line.strip() for line in f.readlines()]

OUTPUT_LAYERS = [MODEL.getLayerNames()[i[0] - 1]
                 for i in MODEL.getUnconnectedOutLayers()]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
COLORS /= (np.sum(COLORS ** 2, axis=1) ** 0.5 / 255)[np.newaxis].T


def detectObj(snap):
    height, width, channels = snap.shape
    blob = cv2.dnn.blobFromImage(
        snap, 1 / 255, (416, 416), swapRB=True, crop=False)

    MODEL.setInput(blob)
    outs = MODEL.forward(OUTPUT_LAYERS)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                # Object detected
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            print(str(CLASSES[class_ids[i]]))
            x, y, w, h = boxes[i]
            label = str(CLASSES[class_ids[i]])
            color = COLORS[i]
            cv2.rectangle(snap, (x, y), (x + w, y + h), color, 2)
            cv2.putText(snap, label, (x, y - 5), font, 2, color, 2)
    return snap


def show():
    while (VIDEO.isOpened()):
        ret, snap = VIDEO.read()
        if _flipH:
            snap = cv2.flip(snap, 1)

        if ret == True:
            if _preview:
                # snap = cv2.resize(snap, (0, 0), fx=0.5, fy=0.5)
                if _detect:
                    snap = detectObj(snap)

            else:
                snap = np.zeros((
                    int(VIDEO.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                    int(VIDEO.get(cv2.CAP_PROP_FRAME_WIDTH))
                ), np.uint8)
                label = 'camera disabled'
                H, W = snap.shape
                font = cv2.FONT_HERSHEY_PLAIN
                color = (255, 255, 255)
                cv2.putText(snap, label, (W // 2 - 100, H // 2),
                            font, 2, color, 2)

            # cv2.imshow('frame', snap)
            # if cv2.waitKey(1) & 0xFF == ord('q'):
            #     break

            frame = cv2.imencode('.jpg', snap)[1].tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
            time.sleep(0.01)

        else:
            break
    print('off')


# show()
